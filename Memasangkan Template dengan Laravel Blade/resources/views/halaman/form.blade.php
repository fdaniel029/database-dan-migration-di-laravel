@extends('layout.master')

@section('judul')
<h1>Buat Account Baru!</h1>
@endsection


@section('content')
<h3>Sign up form</h3>

<form action="/welcome" method="post">
    @csrf
  <label for="fname">First name: </label><br>
  <input type="text" id="fname" name="fname"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname"><br><br>


  <label for="gender"> Gender: </label><br>
  <input type="radio" id="gender" name="gender" > Male <br>
  <input type="radio" id="gender" name="gender" > Female <br>
  <input type="radio" id="gender" name="gender"> Other <br><br>

  <label for="gender"> Nationally: </label><br><br>
	<select name="Country" id="Country">
  <option value="Indonesian">Indonesian</option>
	</select><br><br>

	<label for="Language"> Language Spoken: </label><br><br>
	<input type="checkbox" id="Language" name="Language" > Bahasa Indonesia <br>
	<input type="checkbox" id="Language" name="Language" > English <br>
	<input type="checkbox" id="Language" name="Language" > Other <br><br>

	<label for="Bio"> Bio: </label><br>
	<textarea id="Bio" name="Bio" rows="4" cols="50">
	</textarea><br>
    <input type="submit" value="Sign Up">
    @endsection
